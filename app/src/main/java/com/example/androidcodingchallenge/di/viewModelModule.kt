package com.example.androidcodingchallenge.di

import com.example.androidcodingchallenge.ui.MainViewModel
import org.koin.dsl.module

val viewModelModule= module {
    factory {
        MainViewModel(get())
    }
}