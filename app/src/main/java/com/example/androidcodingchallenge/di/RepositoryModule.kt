package com.example.androidcodingchallenge.di

import com.example.androidcodingchallenge.repository.MainRepository
import org.koin.dsl.module

val repositoryModule= module {
    single {
        MainRepository(get(),get())
    }
}