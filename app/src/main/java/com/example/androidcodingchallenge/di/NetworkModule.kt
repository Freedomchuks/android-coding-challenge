package com.example.androidcodingchallenge.di

import com.example.androidcodingchallenge.utils.LiveDataCallAdapterFactory
import com.example.androidcodingchallenge.api.ApiServices
import com.example.androidcodingchallenge.utils.Constant.Companion.BASE_URL
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule= module {
        single { Gson() }

        val httpInterceptor= HttpLoggingInterceptor()
        httpInterceptor.apply {
            level= HttpLoggingInterceptor.Level.BODY
        }

        single {
            OkHttpClient.Builder()
                .addInterceptor(httpInterceptor)
                .build()
        }

        single {
            Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .client(get())
                .build()
                .create(ApiServices::class.java)
        }
}