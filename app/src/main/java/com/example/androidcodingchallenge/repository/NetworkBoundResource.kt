package com.example.androidcodingchallenge.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.example.androidcodingchallenge.utils.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch

/** abstract class that handles network business logics**/
abstract class NetworkBoundResource<ResultType,ResponseType>internal  constructor(
    isNetworkAvailable:Boolean?=null
){
    private val result:MediatorLiveData<Resource<ResultType>> = MediatorLiveData()

    init {
        result.postValue(Resource.Loading())
        if (isNetworkAvailable!!){
            CoroutineScope(Main).launch {
                fetchFromApi()
            }
        }else{
            result.postValue(Resource.Error("check your internet connection"))
        }

    }

    private fun fetchFromApi(){
        val apiService = fetchService()
        result.addSource(apiService){
            result.removeSource(apiService)

            it.let {
                when(it){
                    is ApiEmptyResponse -> {
                        setValue(Resource.Error("auth-token expired"))
                    }
                    is ApiSuccessResponse -> {
                        setValue(Resource.Success(it.body))
                    }
                    is ApiErrorResponse -> {
                        setValue(Resource.Error(it.errorMessage))
                    }
                }
            }

        }

    }

    // Called to create the API call.
    protected abstract  fun fetchService(): LiveData<ApiResponse<ResultType>>

    // Returns a LiveData object that represents the resource that's implemented
    // in the base class.
    fun asLiveData(): LiveData<Resource<ResultType>> {
        return result
    }

    private fun setValue(newValue: Resource<ResultType>) {
        result.value = newValue
    }
}