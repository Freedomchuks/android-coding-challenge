package com.example.androidcodingchallenge.repository

import android.app.Application
import androidx.lifecycle.LiveData
import com.example.androidcodingchallenge.api.ApiServices
import com.example.androidcodingchallenge.model.BIN
import com.example.androidcodingchallenge.utils.ApiResponse
import com.example.androidcodingchallenge.utils.Resource
import com.example.androidcodingchallenge.utils.isConnectedToTheInternet

class MainRepository(val apiServices: ApiServices,val application: Application){

    fun getCardDetails(cardNumber:String):LiveData<Resource<BIN>>{
        return object :NetworkBoundResource<BIN,BIN>(application.isConnectedToTheInternet()){
            override fun fetchService(): LiveData<ApiResponse<BIN>> {
                return apiServices.getCard(cardNumber)
            }

        }.asLiveData()
    }

}