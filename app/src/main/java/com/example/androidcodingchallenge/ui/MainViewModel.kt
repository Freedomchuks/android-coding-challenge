package com.example.androidcodingchallenge.ui

import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.example.androidcodingchallenge.R
import com.example.androidcodingchallenge.model.BIN
import com.example.androidcodingchallenge.repository.MainRepository
import com.example.androidcodingchallenge.utils.AbsentLiveData
import com.example.androidcodingchallenge.utils.Event
import com.example.androidcodingchallenge.utils.Resource


class MainViewModel(val repository: MainRepository): ViewModel(){

    private val data:MutableLiveData<String> = MutableLiveData()
    var notification=MutableLiveData<Event<Int>>()
    var binNumber= MutableLiveData<String>()
    var progressbar=MutableLiveData<Int>()

    fun validateBinNumber()    {
        when{
            binNumber.value.isNullOrEmpty()->{sendNotificationToUi(R.string.Bin_Number_Cannot_Be_Empty)}
            binNumber.value!!.length<6->{sendNotificationToUi(R.string.Bin_Number_Must_Be_Greater_Than_6)}
            else->{
                setOnClick("shoot")
                progressbar.value=0
            }
        }
    }

    val attemptCall: LiveData<Resource<BIN>> = data.switchMap {
        if (it.isNullOrEmpty()){
            AbsentLiveData.create()
        }else {
            repository.getCardDetails(binNumber.value!!)
        }
    }

    fun setOnClick(newTrigger:String){
        data.value=newTrigger
    }

    /** mvvm method of sending event to Ui without interfaces
     * refrence
     * https://medium.com/@freedom.chuks7/handling-events-the-mvvm-way-4c6ab6b93f20
     * **/
    private fun sendNotificationToUi(@StringRes data:Int){
        notification.value= Event(data)
    }

}