package com.example.androidcodingchallenge.ui

import android.os.Bundle
import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.androidcodingchallenge.R
import com.example.androidcodingchallenge.databinding.ActivityMainBinding
import com.example.androidcodingchallenge.model.BIN
import com.example.androidcodingchallenge.utils.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {
    lateinit var binding:ActivityMainBinding
    lateinit var viewModel: MainViewModel
    private val mainViewModel:MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_main)
        viewModel=ViewModelProvider(this)[mainViewModel::class.java]
        binding.viewmodel=viewModel
        subsribeObserver()
    }


    private fun subsribeObserver() {

        viewModel.notification.observe(this, Observer {
            it?.getContentIfNotHandled()?.let {
                hideSoftKeyBoard()
                binding.root.snack(it)
            }
        })

        viewModel.attemptCall.observe(this, Observer{ res->
            hideSoftKeyBoard()
            when(res){
                is Resource.Success -> {
                    displayData(res.data!!)
                }
                is Resource.Loading -> {binding.loader.visibility= VISIBLE}

                is Resource.Error -> {showDialog("error",res.message.toString());binding.loader.visibility=GONE}
            }
        })
    }

    fun displayData(data:BIN){
        binding.loader.visibility=GONE
        binding.result.text="CardBrand: ${data.cardBrand}\n CardCategory:  ${data.cardCategory} \n CardType:   ${data.cardType}" +
                "\n CardCountry:   ${data.country} \n CardCurrencyCode:   ${data.currencyCode} \n CardIssuer:   ${data.issuer} \n CardIssuerNumber:   ${data.issuerPhone}" +
                "\n CardValidity :  ${data.valid}"
    }




}
