package com.example.androidcodingchallenge

import android.app.Application
import com.example.androidcodingchallenge.di.networkModule
import com.example.androidcodingchallenge.di.repositoryModule
import com.example.androidcodingchallenge.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

//application initialization point

class BaseApplication:Application(){
    override fun onCreate() {
        super.onCreate()
        startKoin {
            printLogger()
            androidContext(this@BaseApplication)
            modules(listOf(networkModule, repositoryModule, viewModelModule))
        }
    }
}
