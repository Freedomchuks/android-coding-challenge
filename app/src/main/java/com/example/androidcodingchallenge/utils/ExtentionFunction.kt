package com.example.androidcodingchallenge.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.ContentValues
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.IntegerRes
import androidx.annotation.NonNull
import com.afollestad.materialdialogs.MaterialDialog
import com.example.androidcodingchallenge.R
import com.google.android.material.snackbar.Snackbar


/** connection manager **/
    @NonNull
    fun Application.isConnectedToTheInternet(): Boolean{
        val cnxManager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        try{
            val netInfo : NetworkInfo? = cnxManager.activeNetworkInfo
            return netInfo?.isConnectedOrConnecting ?: false
        }catch (e: Exception){
            Log.e(ContentValues.TAG, "isConnectedToTheInternet: ${e.message}")
        }
        return false
    }

    /** material dialog extension **/
    fun Activity.showDialog(title:String, message:String){
        MaterialDialog(this).show {
            cornerRadius(5F)
            title(text = title)
            message(text = message)

            positiveButton(R.string.agree) { dialog ->
                hide()
            }

            negativeButton {  }

        }


    }

/** toast extension **/
    fun Context.showToast(message: String){
        Toast.makeText(this,message,Toast.LENGTH_LONG).show()
    }

/** Hide SoftKeyboard Extension **/
    fun Activity.hideSoftKeyBoard(){
        val view=this.currentFocus
        view?.let {
            val imm=getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.hideSoftInputFromWindow(it.windowToken,0)
        }
    }

/** Hide Snackbar Extension **/
@SuppressLint("ResourceType")
fun View.snack(@IntegerRes message:Int){
    Snackbar.make(this,resources.getString(message),Snackbar.LENGTH_LONG).setAction(
        "okay",View.OnClickListener {
            it.alpha=0.3F
        }
    ).show()
}