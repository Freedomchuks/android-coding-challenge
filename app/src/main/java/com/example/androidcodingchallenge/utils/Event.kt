package com.example.androidcodingchallenge.utils

/** class to implemant a one shot use of data and as a medium to send
 * event to the ui using the mvvm architecture
 *
 * Refrence https://medium.com/@freedom.chuks7/handling-events-the-mvvm-way-4c6ab6b93f20
 */
open class Event<out T>(private val content:T) {
    private var hasBeenHandled=false

    /**
     * Returns the content and prevent it use again
     * **/
    fun getContentIfNotHandled():T? {
        return (if (hasBeenHandled){
            null
        }else{
            hasBeenHandled=true
            content
        })
    }
    /**
     * Returns the content, even if it's already handled
     * **/
    fun peekContent():T =content

}