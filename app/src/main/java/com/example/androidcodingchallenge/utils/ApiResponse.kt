package com.example.androidcodingchallenge.utils

import android.util.Log
import org.json.JSONObject
import retrofit2.Response


/** Generic Api Handler Class**/

@Suppress("unused") // T is used in extending classes
sealed class ApiResponse<T> {

    companion object {
        private const val TAG: String = "AppDebug"

        fun <T> create(error: Throwable): ApiErrorResponse<T> {
            return ApiErrorResponse(
                error.message ?: "unknown error"
            )
        }

        fun <T> create(response: Response<T>): ApiResponse<T> {

            when {
                response.isSuccessful -> {
                    val body = response.body()
                    return if (body == null || response.code() == 204) {
                        ApiEmptyResponse()
                    } else{
                        ApiSuccessResponse(body = body)
                    }

                }
                response.code()==401 -> {
                    return ApiErrorResponse("401 Unauthorized. Token may be invalid.")
                }
                else -> {
                    val jObjError = JSONObject(response.errorBody()!!.string())
                    return ApiErrorResponse(jObjError.getString("message") ?: "unknown error")

                }
            }
        }


    }
}

/**
 * separate class for HTTP 204 responses so that we can make ApiSuccessResponse's body non-null.
 */
class ApiEmptyResponse<T> : ApiResponse<T>()

data class ApiSuccessResponse<T>(val body: T) : ApiResponse<T>()

data class ApiErrorResponse<T>(val errorMessage: String) : ApiResponse<T>()



















