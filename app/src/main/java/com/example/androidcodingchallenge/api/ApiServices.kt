package com.example.androidcodingchallenge.api

import androidx.lifecycle.LiveData
import com.example.androidcodingchallenge.model.BIN
import com.example.androidcodingchallenge.utils.ApiResponse
import com.example.androidcodingchallenge.utils.Constant.Companion.API_KEY
import com.example.androidcodingchallenge.utils.Constant.Companion.BASE_URL
import com.example.androidcodingchallenge.utils.Constant.Companion.USER_ID
import retrofit2.http.*

interface ApiServices {

    @GET("https://neutrinoapi.net/bin-lookup")
    @Headers("api-key: $API_KEY",
        "user-id: $USER_ID"
    )
    fun getCard(@Query("bin-number") BinNumber:String): LiveData<ApiResponse<BIN>>
}