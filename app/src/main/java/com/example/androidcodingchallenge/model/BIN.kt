package com.example.androidcodingchallenge.model

import com.google.gson.annotations.SerializedName

data class BIN(
    @SerializedName("card-brand") val cardBrand: String,
    @SerializedName("card-category") val cardCategory: String,
    @SerializedName("card-type") val cardType: String,
    @SerializedName("country") val country: String,
    @SerializedName("country-code") val countryCode: String,
    @SerializedName("country-code3") val countryCode3: String,
    @SerializedName("currency-code") val currencyCode: String,
    @SerializedName("issuer") val issuer: String,
    @SerializedName("issuer-phone") val issuerPhone: String,
    @SerializedName("issuer-website") val issuerWebsite: String,
    @SerializedName("valid") val valid: Boolean
)