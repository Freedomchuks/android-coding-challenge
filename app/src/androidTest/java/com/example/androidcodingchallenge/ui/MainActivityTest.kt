package com.example.androidcodingchallenge.ui


import android.os.SystemClock
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.example.androidcodingchallenge.R
import org.hamcrest.CoreMatchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4ClassRunner::class)
class MainActivityTest{

    @get: Rule
    val activityRule=ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun testActivityInView() {
        onView(withId(R.id.main)).check(matches(isDisplayed()))
    }

    @Test
    fun testVisibilityOfButtonAndTextField() {
        //Testing for visibility for button
        onView(withId(R.id.btn)).check(matches(isDisplayed()))

        //Testing for visibility for textfield
        onView(withId(R.id.textfield)).check(matches(isDisplayed()))
    }

    @Test //test click without data in the feild
    fun testValidation() {

        //pass if button isclicked
        onView(withId(R.id.btn)).perform(click())

        //pass if the edittext does not mtch the empty string
        onView(withId(R.id.textfield)).check((matches(not(withText("")))))

        //pass if snackbar is displayed
        onView(withId(com.google.android.material.R.id.snackbar_text))
            .check(matches(isDisplayed()))
    }

    @Test
     fun testForResultInView() {

        //pass if the edittext is not empty
        onView(withId(R.id.editText)).perform(clearText(), typeText("1122334455"))

        //pass if button isclicked
        onView(withId(R.id.btn)).perform(click())

        //pass result is in view
        SystemClock.sleep(2000)
        onView(withId(R.id.result)).check(matches(isDisplayed()))
    }
}